# prometheus-monitoring-demo

## Getting started with helm

You should have minikube or the target kubernetes cluster already set up.

Commands below are relative to the cloned repository.

```
brew install helm
helm init
helm install --dep-up <this repository>
```

Currently there are no included dashboards. If you find a nice one it can be
added to the project by default.

If you do not have dynamic persistent volumes available, apply:
```
---
kind: PersistentVolume
apiVersion: v1
metadata:
  name: prometheus-pv-volume
  labels:
    type: local
spec:
  storageClassName: prometheus
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data/prometheus"
---
kind: PersistentVolume
apiVersion: v1
metadata:
  name: alertmanager-pv-volume
  labels:
    type: local
spec:
  storageClassName: alertmanager
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data/alertmanager"
```

And add the following values with helm:
```
prometheus:
  server:
    persistentVolume:
      storageClass: alertmanager
  alertmanager:
    persistentVolume:
      storageClass: prometheus
```
